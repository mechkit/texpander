#!/bin/bash

# Version: 2.0
# Release: November 24, 2017

# Get window id, pass to getwindow pid to output the pid of current window
pid=$(xdotool getwindowfocus getwindowpid)

# Store text name of process based on pid of current window
proc_name=$(cat /proc/$pid/comm)


if [ -n "$1" ]
then
  texpander_path=$1
else
  texpander_path=${HOME}/.texpander
  # If ~/.texpander directory does not exist, create it
  if [ ! -d ${texpander} ]; then
      mkdir ${texpander}
  fi
fi


# Store base directory path, expand complete path using HOME environemtn variable
base_dir=$(realpath "${texpander_path}")

# Set globstar shell option (turn on) ** for filename matching glob patterns on subdirectories of ~/.texpander
# shopt -s globstar

# Find regular files in base_dir, pipe output to sed
# abbrvs=$(find "${base_dir}" -type f | sort | sed "s?^${base_dir}/??g" )
abbrvs=$( ls -1F "${base_dir}" )

name=$(zenity --list --title=Texpander --width=300 --height=800 --column=Abbreviations "_" $abbrvs)

# Exit if no name is reterned (dialog canceled)
if [ -z $name ] && exit 1

path="${base_dir}/${name}"

if [ -f "${base_dir}/${name}" ] || [ -d "${base_dir}/${name}" ]
then
  if [ -e "$path" ]
  then

    if [ -d "$path" ]
    then 
      $BASH_SOURCE $path
    elif [ -x "$path" ]
    then
      output=$( $path )
    else 
      output=$( cat "$path" )
    fi

    # Preserve the current value of the clipboard
    clipboard=$(xsel -b -o)

    # Put text in primary buffer for Shift+Insert pasting
    echo -n "$output" | xsel -p -i

    # Put text in clipboard selection for apps like Firefox that 
    # insist on using the clipboard for all pasting
    echo -n "$output" | xsel -b -i

    # Paste text into current active window
    sleep 0.3
    xdotool key shift+Insert

    # If you're having trouble pasting into apps, use xdotool
    # to type into the app instead. This is a little bit slower
    # but may work better with some applications.
    #
    # Make xdotool type RETURN instead of LINEFEED characters 
    # otherwise some apps like Gmail in Firefox won't recognize
    # newline characters.
    #
    # To use this, comment out line #32 (xdotool key shift+Insert)
    # and uncomment the line below.
    #xdotool type -- "$(xsel -bo | tr \\n \\r | sed s/\\r*\$//)"

    # Restore the original value of the clipboard
    # sleep 0.5
    # echo $clipboard | xsel -b -i
    
    xdotool key Escape
    xdotool key Escape


  else
    zenity --error --text="Abbreviation not found:\n${name}"
  fi
fi
